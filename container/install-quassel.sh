#/bin/sh

set -e

apk add --update quassel-core

mkdir -p /var/lib/quassel

/container/user-cleanup.sh
/container/upgrade.sh

