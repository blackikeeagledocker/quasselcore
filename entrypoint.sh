#!/bin/sh

/container/user-setup.sh

chown $C_USER:$C_USER /var/lib/quassel
exec su-exec $C_USER:$C_USER "$@"
